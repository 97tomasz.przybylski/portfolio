<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Booking_view</title>

        <link rel="stylesheet" type="text/css" href="every.css">

    </head>
    <body style="background-color: beige">

        <img src="bus-banner.jpg" width=100%><br>

        <form>
        <div class="topnav">
            <a href="search.php">Home</a>
            <a href="vehicle_view.php">Vehicles</a>
            <a href="bookingvehicle.php">Booking</a>
            <a href="basket.php">Basket</a>
            <a href="about.php">About</a>
            <a href="contact.php">Contact</a>
        </div>

        <table>
            <thead>
                <tr>
                    <th>Vehicle ID</th>
                    <th>Model ID</th>

                </tr>
            </thead>
            <tbody>
                <?php foreach ($results as $booking): ?>
                <tr>
                    <td><?= $booking->VehicleID ?></td>
                    <!-- <td><?= $booking->ModelID?></td> -->
                </tr>
                <?php endforeach ?>
            </tbody>
        </table>

        <div class="footer">
                        <p>Berwyn Bus Hire Company Ltd</p>
                        <p>K1602155 / K1834977 / K1524638 / K1823571 / K1515883 / K1709948</p>
        </div>
    </body>
</html>