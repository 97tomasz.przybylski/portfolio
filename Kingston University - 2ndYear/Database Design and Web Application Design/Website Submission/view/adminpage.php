<?php require_once "../controller/vehicle_list.php"; 
require_once "../controller/vehicle_model.php";
require_once "../model/vehicles.php";?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Berwyn Bus Vehicles</title>

        <link rel="stylesheet" type="text/css" href="every.css">

    </head>

<body>
    <img src="bus-banner.jpg"><br>

        <div class="topnav">
            <a href="index.php">Home</a>
            <a href="vehicle.php">Vehicles</a>
            <a href="booking.php">Booking</a>
            <a href="basket.php">Basket</a>
            <a href="about.php">About</a>
            <a href="contact.php">Contact</a>
            <a href="register.php">Register</a>
            <a href="../controller/logout.php">Log Out</a>
    </div>
 
    <h1> Admin Page </h1>
    <h2> Manage Vehicles  </h2> 
    <table id = "allVehiclesTable">
  <thead>
  <tr>

  <th></th>
  <th>Vehicle ID </th>
  <th>Model ID </th>
  <th>Vehicle Model</th>
  <th>Number Of Vehicles</th>
  <th>Passengers</th>
  <th>Driving License</th>
  <th>Hourly Rate </th>
  </tr>
  </thead>
  <tbody>
  <?php foreach($VehicleResults as $vehicle): ?>
    <tr>
  <td><?= $vehicle->VehicleID ?> </td>
  <td><?= $vehicle->ModelID ?> </td>
  <td><?= $vehicle->VehicleModel ?></td>
  <td><?= $vehicle->NumberOfVehicles ?> </td>
  <td><?= $vehicle->Passengers ?> </td>
  <td><?= $vehicle->DrivingLicense ?></td>
  <td><?= $vehicle->HourlyRate ?>  </td>

 
  <td><a id = "<?= $vehicle->VehicleID ?>" href="#editVehicle" onclick="GetCellValues(<?= $vehicle->VehicleID ?>)"> Edit </a></td>

  </tr>
  <?php endforeach ?>
  </tbody>
  </table>



<hr>
    <h3>Add new vehicle</h3>

<form method="post" action="../controller/adminpage.php" >
  VehicleID: <input name="VehicleID" required><br/>
  ModelID: <input name="ModelID" required><br/>

 
  <input type="submit" >
</form>
<hr>

<h3>Delete Vehicles</h3>

<form method="post" action="../controller/adminpage.php">
Delete one of your vehicle (please enter vehicle ID)
<input name="delete" required>
<input type="submit" value= "Delete!" >
</form>


<hr>

<h2>Edit Vehicles</h2>
<form id = "editVehicle" method="post" action="..controller/adminpage.php">
  <input id = "chosenVehicle" type = "hidden" name="chosenVehicle" required><br/>
  Vehicle ID <input id = "editVehicleID" name="editVehicleID" required><br/>
  Model ID: <input id = "editModelID" name="editModelID" required><br/>
  Vehicle Model: <input id = "editVehicleModel" name="editVehicleModel" required><br/>
  Number Of Vehicles: <input id = "editNumberOfVehicles"  name="editNumberOfVehicles" required><br/>
  Passengers: <input id = "editPassengers"  name="editPassengers" required><br/>
  Driving License: <input id = "editDrivingLicense" name="editDepartureTime" required><br/>
  Hourly Rate: <input id = "editHourlyRate"  name="editHourlyRate" required><br/>
<input id = "editButton" name = "editButton" type="submit" value="Save" disabled="true">
</form>



                </tbody>
        </table>
        <br>
        <div class="footer">
            <p>Berwyn Bus Hire Company Ltd</p>
            <p>K1602155 / K1834977 / K1524638 / K1823571 / K1515883 / 9948K170</p>
        </div>
    </form>

</body>
<footer>

</footer>                