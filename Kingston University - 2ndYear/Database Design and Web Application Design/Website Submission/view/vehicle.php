<?php require_once "../controller/vehicle_list.php"; 
require_once "../controller/vehicle_model.php";
require_once "../controller/log.php";?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Berwyn Bus Vehicles</title>
        <script type="text/javascript" src="../controller/sortTable.js"></script>
        <link rel="stylesheet" type="text/css" href="every.css">
    </head>
<body>
    <img src="bus-banner.jpg"><br>
    <form>
    <div class="topnav">
            <a href="index.php">Home</a>
            <a href="vehicle.php">Vehicles</a>
            <a href="booking.php">Booking</a>
            <a href="basket.php">Basket</a>
            <a href="about.php">About</a>
            <a href="contact.php">Contact</a>
            <a href="register.php">Register</a>
            <a href="userLogin.php">Login</a>
        </div>
        </div>
        <form method="post" action="">   
                          
        <label for="searchtype"> Search for Vehicle by: </label>
        <select id="searchtype" name="searchtype"> 
            <option value="cost">Cost</option>
            <option value="passenger">Passenger</option>
            <option value="model">Bus Model</option>
            <option value="license">Driving License</option>
        </select>
        </form>  
        <p1> Below is a list of all of our vehicles and all the details needed for it </p1> <br>
        <tr>
        <table class="container" id="sortBytable">
            <thead>
                <tr>
                    <th>Vehicle ID</th>
                    <th>Model ID</th>
                    <th>Model Name</th>
                    <th>Number Of Vehicles</th>
                    <th>Passenger Limit</th>
                    <th>Driving License Required</th>
                    <th>Hourly Rate</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($VehicleResults as $vehicle): ?>
                <tr>
                    <td><?= $vehicle->VehicleID?></td>
                    <td><?= $vehicle->ModelID?></td>
                    <td><?= $vehicle->VehicleModel ?></td>
                    <td><?= $vehicle->NumberOfVehicles ?></td>
                    <td><?= $vehicle->Passengers ?></td>
                    <td><?= $vehicle->DrivingLicense ?></td>
                    <td>£<?= $vehicle->HourlyRate ?></td>
                    <td><form method = "post" action="../controller/addToBasket.php">
                    <input type = "hidden" name='VehicleID' value="<?$vehicle->VehicleID?>"/>
                    <input type ="submit" value="Add to Basket"/>
                    </form>
                <?php endforeach ?>
            </tbody>
        </table>
        <br>
        <div class="footer">
            <p>Berwyn Bus Hire Company Ltd</p>
            <p>K1602155 / K1834977 / K1524638 / K1823571 / K1515883 / K1709948</p>
        </div>
    </form>

</body>
<footer>

</footer>