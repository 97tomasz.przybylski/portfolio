<?php require_once "../controller/vehicle_list.php"; 
require_once "../controller/vehicle_model.php";?>
<!DOCTYPE html>
<html>
<head>
    <title>Contact Us</title>
    <link rel="stylesheet" type="text/css" href="every.css">

</head>
<body>

    <img src="bus-banner.jpg" width="100%">

        <div class="topnav">
            <a href="index.php">Home</a>
            <a href="vehicle.php">Vehicles</a>
            <a href="booking.php">Booking</a>
            <a href="basket.php">Basket</a>
            <a href="about.php">About</a>
            <a href="contact.php">Contact</a>
            <a href="register.php">Register</a>
            <a href="userLogin.php">Login</a>
        </div>

    <p>We will speedily and kindly answer any question you may have about our online renting service right here</p>
    <h3>Contact Form</h3>

    <div class="container">
        <form action="/action_page.php">
            <label for="fname">First Name</label>
            <input type="text" id="fname" name="firstname" placeholder="Your name..">

            <label for="lname">Last Name</label>
            <input type="text" id="lname" name="lastname" placeholder="Your last name..">

            <label for="email">Email</label>
            <input type="text" id="email" name="emails" placeholder="Your email..">

            <label for="country">Country</label>
            
            <select id="country" name="country">
            <?php foreach ($VehicleResults as $vehicle): ?>
                <option value="<?$vehicle->ModelID?>"><?$vehicle->$ModelID?></option>
            <?php endforeach ?>
            <option value="eu">European Union</option>
            <option value="uk">United Kingdom</option>
            <option value="other">Other</option>
            </select>

            <label for="subject">Subject</label>
            <textarea id="subject" name="subject" placeholder="Write something.." style="height:200px"></textarea>

            <input type="submit" value="Submit" >
            <br>
            <br>

        <div class="footer">
            <p>Berwyn Bus Hire Company Ltd</p>
            <p>K1602155 / K1834977 / K1524638 / K1823571 / K1515883 / K1709948</p>
        </div>
        </form>
</body>
<footer>

</footer>