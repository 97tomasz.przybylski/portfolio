<html>
<head>
    <title>Login Page</title>
    
    <link rel="stylesheet" type="text/css" href="every.css">

</head>
<body>
        <img src="bus-banner.jpg" width=100%>
        <div class="topnav">
            <a href="index.php">Home</a>
            <a href="vehicle.php">Vehicles</a>
            <a href="booking.php">Booking</a>
            <a href="basket.php">Basket</a>
            <a href="about.php">About</a>
            <a href="contact.php">Contact</a>
            <a href="register.php">Register</a>
            <a href="userLogin.php">Login</a>
        </div>
        </div>

<?php if(isset($_SESSION["errormsg"])):?>
<?= $_SESSION["errormsg"]?>
<?php $_SESSION["errormsg"] = ""; ?>
<?php endif?>

    <h2>Login Form</h2>
   
 <?php if(isset($_SESSION["userLoggedInMessage"]) && $_SESSION["userLoggedInMessage"] == "Wrong log-in details or user doesn't exist"): ?>
<p><font color="red"> <?= $_SESSION["userLoggedInMessage"] ?> </font></p>
<?php elseif(isset($_SESSION["userLoggedInMessage"]) && $_SESSION["userLoggedInMessage"] == "Succesfully logged in"): ?>
<p><font color="green"> <?= $_SESSION["userLoggedInMessage"] ?> </font></p>
<?php endif ?>

    <form method="POST" action="../controller/login.php">
            <div class="container">
                    <label for="Username"><b>Username</b></label>
                    <input type="text" placeholder="Enter Username" name="Username" required><br>
                    <label for="Password"><b>Password</b></label>
                    <input type="password" placeholder="Enter Password" name="Password" required><br><br>
                    <input type="submit"><br><br>
                    <label>
                      <input type="checkbox" checked="checked" name="remember"> Remember Me *placeholder*
                    </label>
                  </div>
            <div class="footer">
                        <p>Berwyn Bus Hire Company Ltd</p>
                        <p>K1602155 / K1834977 / K1524638 / K1823571 / K1515883 / K1709948</p>
                </div>
        </form>
        <a href="register.php">Register</a>
</body>    
</html>