$(document).ready(function()
{
    console.log("ready"); //debug purposes
    $("select").change(function()
    {
        console.log("change"); //debug purposes
        var switching, i, x, y, shouldSwitch,location;
        var table = document.getElementById("sortByTable");
        console.log("elementid1"); //debug purposes
        var selected = $("select option:selected").val();
        if (selected == "cost") 
        {
            location = 6;
        } else if (selected =="passenger"){
            location = 4;
        } else if (selected =="model"){
            location = 2;
        } else if (selected =="license"){
            location = 5;
        }
        console.log("elseif"); //debug purposes
        switching = true;
        while (switching) 
        {
            switching = false;
            var trows = table.rows;
            for (i = 1; i < (trows.length - 1); i++) 
            {
                shouldSwitch = false;
                x = trows[i].getElementsByTagName("TD")[location];
                y = trows[i + 1].getElementsByTagName("TD")[location];
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) 
                {
                    shouldSwitch = true;
                    break;
                }
            }
            if (shouldSwitch) 
            {
                trows[i].parentNode.insertBefore(trows[i + 1], trows[i]);
                switching = true;
            }
        }
    });
});