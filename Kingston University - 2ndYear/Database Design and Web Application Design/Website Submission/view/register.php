<html>
<head>
    <title>Register Page</title>
    <link rel="stylesheet" type="text/css" href="every.css">
</head>
<body>
    <img src="bus-banner.jpg" width=100%>
    <div class="topnav">
            <a href="index.php">Home</a>
            <a href="vehicle.php">Vehicles</a>
            <a href="booking.php">Booking</a>
            <a href="basket.php">Basket</a>
            <a href="about.php">About</a>
            <a href="contact.php">Contact</a>
            <a href="register.php">Register</a>
            <a href="userLogin.php">Login</a>
        </div>
    <form>
        <div class="container">
        <h2>Register</h2>
        <p>Please fill in the form to create an account.</p><br>

        <label for="firstname"><b>First Name</b></label>
        <input type="text" placeholder="Enter first name" required><br>

        <label for="lastname"><b>Last Name</b></label>
        <input type="text" placeholder="Enter last name" required><br>

        <label for="email"><b>Email</b></label>
        <input type="text" placeholder="Enter Email Address" required><br>
        
        <label for="password"><b>Password</b></label>
        <input type="password" placeholder="Enter Password" required><br>

        <label for="confirmpassword"><b>Confirm Password</b></label>
        <input type="password" placeholder="Enter password to confirm" required><br><br>

        <button type="submit" class="registerbtn"><a href="welcomepage.html">Register</a></button>
        </div>

        <div class="container signin">
            <p>Already have an account?<a href="loginpage.html">Sign In</a></p>
        </div>

        <div class="footer">
                <p>Berwyn Bus Hire Company Ltd</p>
                <p>K1602155 / K1834977 / K1524638 / K1823571 / K1515883 / K1709948</p>
        </div>

    </form>
</body>    
</html>