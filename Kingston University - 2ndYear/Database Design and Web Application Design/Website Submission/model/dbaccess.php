<?php
#if there is no SESSION, start a new session. Otherwise, move on
 require_once "vehicles.php";
 require_once "vehicle_Model.php";
 require_once "vehicleFull.php";
 require_once "user.php";
 require_once "admin.php";
if (session_status() ==PHP_SESSION_NONE){
    session_start();
}
$db = "mysql:host=kunet;dbname=db_k1524638";
$userName = "k1524638"; #username to the PHPmyAdmin for KUNet
$password = "k1524638"; #password to the PHPmyAdmin for KUNet
$pdo = new PDO($db, $userName, $password, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

function getAllVehicles() {
    global $pdo;
    $statement = $pdo->prepare("SELECT * FROM Vehicles");
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_CLASS, "Vehicles");
    return $results;
}
function getAllVehicle_Model() {
    global $pdo;
    $statement = $pdo->prepare("SELECT * FROM Vehicle_Model");
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_CLASS, "vehicle_Model");
    return $results;
}
function getAllCustomer() {
    global $pdo;
    $statement = $pdo->prepare("SELECT * FROM Customer");
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_CLASS, "Customer");
    return $results;
}
function getAllDriver() {
    global $pdo;
    $statement = $pdo->prepare("SELECT * FROM Driver");
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_CLASS, "Driver");
    return $results;
}
function getAllBooking() {
    global $pdo;
    $statement = $pdo->prepare("SELECT * FROM Booking");
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_CLASS, "Booking");
    return $results;
}



function getAllAdmins(){
    global $pdo;
    $statement = $pdo->prepare("SELECT * FROM Admin");
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_CLASS, "Admin");
    return $results;
}
function getAdminUsername(){
    global $pdo;
    $statement = $pdo->prepare("SELECT Username FROM Admin");
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_CLASS, "User");
    return $results;
}
function getAdminPassword(){
    global $pdo;
    $statement = $pdo->prepare("SELECT Password FROM Admin");
    $statement ->execute();
    $results = $statement->fetchAll(PDO::FETCH_CLASS, "Admin");
    return $results;
}
function getVehiclebyCost($minCost, $maxCost){
    global $pdo;
    $statement = $pdo->prepare("SELECT Vehicle_Model.*, Vehicles.* 
        FROM Vehicle_Model as Vehicle_Model 
        INNER JOIN Vehicles as Vehicles on (Vehicle_Model.ModelID =Vehicles.ModelID)
        WHERE HourlyRate>=? AND HourlyRate<=? 
        ORDER BY HourlyRate");
    $statement->execute([$minCost, $maxCost]);
    $results=$statement->fetchAll(PDO::FETCH_CLASS, "Vehicle_Model");
    return $results;
}

function getVehiclebyPassenger($minPass, $maxPass){
    global $pdo;
    $statement = $pdo->prepare("SELECT Vehicle_Model.*, Vehicles.* 
        FROM Vehicle_Model as Vehicle_Model 
        INNER JOIN Vehicles as Vehicles on (Vehicle_Model.ModelID =Vehicles.ModelID)
        WHERE Passengers>=? AND Passengers<=? 
        ORDER BY Passengers");
    $statement->execute([$minPass, $maxPass]);
    $results=$statement->fetchAll(PDO::FETCH_CLASS, "Vehicle_Model");
    return $results;
}

function getVehicleFull(){
    global $pdo;
    $statement = $pdo->prepare("SELECT Vehicle_Model.*, Vehicles.* 
            FROM Vehicle_Model as Vehicle_Model 
            INNER JOIN Vehicles as Vehicles on (Vehicle_Model.ModelID =Vehicles.ModelID)
            ORDER BY Passengers");
    $statement->execute();
    $results=$statement->fetchAll(PDO::FETCH_CLASS, "vehicleFull");
    return $results;

}

function addNewVehicle($VehicleID, $ModelID){
    global $pdo;
    $statement = $pdo->prepare("INSERT INTO Vehicles (VehicleID, ModelID) VALUES (?,?)");
    $statement->execute([$VehicleID, $ModelID]);
    }



function deleteVehicleByID($VehicleID){
    global $pdo;
    $statement = $pdo->prepare("DELETE FROM Vehicles WHERE (VehicleID = ?)");
    $statement->execute([$VehicleID]);
    }
    
function editVehicle($VehicleID, $ModelID, $VehicleModel, $NumberOfVehicles, $Passengers, $DrivingLicense, $HourlyRate){
    global $pdo;
    $statement = $pdo->prepare("UPDATE Vehicles SET VehicleID = ?, ModelID = ?, NumberOfVehicles = ?, Passengers = ?, DrivingLicense = ?, HourlyRate = ?, WHERE VehicleID = ?");
    $statement->execute([$VehicleID, $ModelID, $NumberOfVehicles, $Passengers, $Passengers, $DrivingLicense, $HourlyRate]);
        
    }

function getAllUsers(){
    global $pdo;
    $statement = $pdo->prepare("SELECT * FROM Admin");
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_CLASS, "Admin");
    return $results;
}
function addNewUser($Username,$Password){
    global $pdo;
    $statement = $pdo->prepare("INSERT INTO Admin (Username, Password) VALUES (?,?)");
    $statement->execute([$Username, $Password]);
}
